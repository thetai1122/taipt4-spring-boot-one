package com.taipt4.ioc;

public class MyApp {
	public static void main(String[] args) {
		
		// create the object
		CoachInterface baseballCoach = new BaseballCoach();
		CoachInterface trackCoach = new TrackCoach();
		// use object
		System.out.println(baseballCoach.getDailyWorkout());
		System.out.println(trackCoach.getDailyWorkout());
	}
}
