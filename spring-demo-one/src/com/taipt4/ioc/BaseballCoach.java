package com.taipt4.ioc;

public class BaseballCoach implements CoachInterface {

	@Override
	public String getDailyWorkout() {
		return "Spend 30 minutes on batting practice";
	}
	
}
